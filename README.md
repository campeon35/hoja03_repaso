# Documentacion del ejercicio de Repaso

### Punto 1

1.	Entramos a la carpeta hosts y añadimos repaso.com  
![](Capturas/1.PNG "")  
![](Capturas/2.PNG "")

2.	Creamos la carpeta repaso, y dentro, el fichero index.html  
![](Capturas/3.PNG "")  
![](Capturas/4.PNG "")

3.	Nos posicionamos en /etc/apache2/sites-available 
	*	Copiamos un .conf ya creado para ahorrar tiempo  
![](Capturas/5.PNG "")
	*	Accedemos a el .conf nuevo y lo configuramos para que nos muestre el html que creamos antes  
![](Capturas/6.PNG "")  
	*	__Acordarrse de hacer log nuevos__  
![](Capturas/7.PNG "")  
	*	___Activamos el site que acabamos de crear y reiniciamos apache___  
![](Capturas/8.PNG "")

___Finalmente comprobamos que funciona entrando a repaso.com___  
![](Capturas/9.PNG "")

***
### Punto 2

1.	Volvemos a /etc/apache2/sites-available y entramos de nuevo al .conf que configuramos para añadirle la siguiente línea  
![](Capturas/10.PNG "")

2.	Para comprobar que funciona reiniciamos apache, vamos a la carpeta donte esta index.html y lo cambiamos de nombre para que no lo encuentre  
![](Capturas/11.PNG "")

___Ya podemos comprobar que no encuentra el archivo y entonces nos muestra todo el contenido de la carpeta___    
![](Capturas/12.PNG "")

***
### Punto 3

1.	Creamos la carpeta con mi nombre
![](Capturas/13.PNG "")

2.	Volvemos al .conf y le añadimos un nuevo directory que sea de la nueva carpeta
	*	En ella tenemos que poner el tipo de autentificacion, el mensaje que sale, el archivo donde buscara los usuarios que pueden entrar...  
![](Capturas/14.PNG "")  
	*	En estos momentos buscara los usuarios en un fichero que no existe, por eso lo creamos ahora

3.	Nos posicionamos en /etc/apache2 y hacemos el comando. Luego comprobamos que funciona  
![](Capturas/15.PNG "")

4.	Para poder comprobar que funciona tenemos que crear un archivo dentro de la carpeta privada  
![](Capturas/16.PNG "")  
![](Capturas/17.PNG "")

___Ahora entramos al navegador y comprobamos que funciona la autentificacion___    
![](Capturas/18.PNG "")
![](Capturas/19.PNG "")

***